#!/usr/bin/env python
import random

import pyperclip
import keyboard
import time

import playsound

unused_words = list()
printing = False
last_word = ''
WORDS = open("words.txt").read().splitlines()
print(len(WORDS), 'loaded in mem')


def random_bool() -> bool:
    random_bit = random.getrandbits(1)
    return bool(random_bit)


def get_random_word() -> str:
    word = random.choice(WORDS)
    if word in unused_words:
        return get_random_word()
    word_len = len(word)
    if word_len == 0 or 5 > word_len or '-' in word or "'" in word or '.' in word:
        unused_words.append(word)
        return get_random_word()
    if random_bool():
        paste_to = random.randint(1, word_len-1)
        word = insert_sting_middle(word, paste_to, '_')
    elif random_bool():
        paste_to = random.randint(1, word_len-1)
        new_s = list(word)
        new_s[paste_to] = new_s[paste_to].upper()
        word = ''.join(new_s)
    word = word[0].lower() + word[1:]
    unused_words.append(word)
    return word


def insert_sting_middle(str, pos, word) -> str:
    return str[:pos] + word + str[pos:]


def handlePressedKeys():
    global printing
    if not printing:
        ctrl = keyboard.is_pressed('ctrl')
        if ctrl and (keyboard.is_pressed('c') or keyboard.is_pressed('v')):
            paste = pyperclip.paste().strip()
            if (len(paste) == 0) or (paste in unused_words):
                word = get_random_word()
                pyperclip.copy(word)
                print('"%s" in clipboard' % word)
        elif ctrl and keyboard.is_pressed('p'):
            word = get_random_word()
            pyperclip.copy(word)
            print('printing "%s"' % word)
            printing = True
            keyboard.release('ctrl')
            for char in word:
                keyboard.write(char)
                time.sleep(random.randint(22, 44) * 0.01)
            playsound.playsound('assets\sound\correct.mp3')
            printing = False


def main():
    while True:
        handlePressedKeys()
        time.sleep(0.1)


if __name__ == '__main__':
    main()

